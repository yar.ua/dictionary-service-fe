import { Grammeme } from "../Data/Paradigm";
import { Button, EditableText, Section, TagInput } from "@blueprintjs/core";
import React from "react";

export function GrammemeView(props: {
  grammeme: Grammeme;
  remove: () => void;
  setText: (_: string) => void;
  setCategories: (_: string) => void;
}) {
  const tags = props.grammeme.categories.split(";");
  return (
    <Section className="grammeme-block">
      <div className="id bp5-text-disabled">{props.grammeme.id}</div>
      <EditableText
        className="wordform bp5-text-large"
        placeholder="словоформа"
        multiline={false}
        maxLines={1}
        value={props.grammeme.text}
        onChange={props.setText}
      ></EditableText>
      <TagInput
        className="categories"
        values={tags}
        addOnBlur
        tagProps={{ minimal: true }}
        onChange={(els) => props.setCategories(els.join(";"))}
      ></TagInput>
      <Button onClick={props.remove} minimal icon="trash" />
    </Section>
  );
}

export function AddGrammeme(props: { add: () => void }) {
  return (
    <Section className="grammeme-block">
      <Button onClick={props.add} icon="plus" text="Додати нову грамему" />
    </Section>
  );
}
