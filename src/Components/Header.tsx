import { Alignment, Button, Menu, MenuItem, Navbar, Popover, Spinner } from "@blueprintjs/core";
import React, { useRef, useState } from "react";
import { logOut, useUser } from "../Data/api";
import { Link, useNavigate } from "react-router-dom";

function LogInButton() {
  const { user, isPending } = useUser();
  let className = "bp5-minimal";
  if (isPending) {
    className += " bp5-skeleton";
  }
  let name = "Авторизуватись";
  if (user) {
    name = user.name;
  }
  const navigate = useNavigate();
  function handleLogin() {
    navigate("/api/login/google");
    navigate(0);
  }
  function handleLogOut() {
    logOut().then(() => navigate(0));
  }
  return (
    <>
      <Popover
        minimal
        placement="bottom-end"
        content={
          <Menu>
            {user ? (
              <MenuItem text="Вийти" onClick={handleLogOut} />
            ) : (
              <MenuItem text="За допомогою Google" onClick={handleLogin} />
            )}
          </Menu>
        }
      >
        <Button className={className} icon="user" text={name} />
      </Popover>
    </>
  );
}

export function Header() {
  return (
    <Navbar>
      <Navbar.Group align={Alignment.LEFT}>
        <Navbar.Heading>
          <Link to={"/"}>Яр. Граматичний Словник</Link>
        </Navbar.Heading>
        <Navbar.Divider />
        <Button className="bp5-minimal" icon="new-layers" text="Додати" />
      </Navbar.Group>
      <Navbar.Group align={Alignment.RIGHT}>
        <LogInButton />
      </Navbar.Group>
    </Navbar>
  );
}
