import { createGrammeme, Paradigm, updatedParadigm, UpdatedParadigm } from "../Data/Paradigm";
import {
  Button,
  EditableText,
  FormGroup,
  H3,
  HotkeysTarget2,
  HTMLSelect,
  Section,
  Spinner,
  TagInput,
} from "@blueprintjs/core";
import React, { useEffect, useReducer } from "react";
import { GrammemeView, AddGrammeme } from "./Grammeme";
import { InitialState, modified, reducer } from "../Data/reducer";
import { useParadigm } from "../Data/api";
import type { IconName } from "@blueprintjs/icons";

const SaveButton = (props: { loading: boolean; modified: boolean; onSave: () => void }) => {
  let icon: IconName = "cloud-tick";
  let text = "Синхронізовано";
  let disabled = true;
  if (!props.loading) {
    icon = "rain";
    text = "Синхронізація";
  } else if (props.modified) {
    icon = "cloud-upload";
    text = "Синхронізувати";
    disabled = false;
  }
  return (
    <Button icon={icon} disabled={disabled} onClick={props.onSave}>
      {text}
    </Button>
  );
};

function LoadedParadigmView(props: { paradigm: Paradigm; editable: boolean; saveParadigm: (p: Paradigm) => void }) {
  const [state, dispatch] = useReducer(reducer, props.paradigm, InitialState);

  let saving = false;
  useEffect(() => {
    if (props.paradigm !== state.originalParadigm) {
      dispatch({ type: "reset", paradigm: props.paradigm });
    }
  }, [props.paradigm, state.originalParadigm]);

  function onSave() {
    if (state.modified) {
      props.saveParadigm(state.paradigm);
    }
  }

  const tags = state.paradigm.categories.split(";");
  return (
    <Section style={{ display: "flex", flexDirection: "column", height: "100%" }}>
      <H3 style={{ margin: 10, display: "flex" }}>
        <a href={"/paradigm/" + state.paradigm.id}>Парадигма #{state.paradigm.id}</a>
        {state.modified ? <span>*</span> : null}
        <span style={{ flexGrow: 1 }} />
        <SaveButton loading={props.editable} modified={state.modified} onSave={onSave} />
      </H3>
      <Section
        className="grammeme-block"
        style={{
          flexGrow: 0,
          flexShrink: 0,
        }}
      >
        <div className="id" />
        <FormGroup
          subLabel="Форма"
          labelFor="text-input"
          labelInfo={state.paradigm.text === state.originalParadigm.text ? "" : "*"}
        >
          <EditableText
            className="wordform bp5-text-large"
            placeholder="словоформа"
            multiline={false}
            maxLines={1}
            value={state.paradigm.text}
            onChange={(e) => dispatch({ type: "setText", text: e, id: state.paradigm.id })}
          ></EditableText>
        </FormGroup>
        <FormGroup
          subLabel="Граматичні категорії"
          labelFor="categories-input"
          labelInfo={state.paradigm.categories === state.originalParadigm.categories ? "" : "*"}
        >
          <TagInput
            className="categories"
            values={tags}
            addOnBlur
            tagProps={{ minimal: true }}
            onChange={(els) =>
              dispatch({
                type: "setCategories",
                categories: els.join(";"),
                id: state.paradigm.id,
              })
            }
          ></TagInput>
        </FormGroup>
        <FormGroup subLabel="Частина мови" labelInfo={state.paradigm.pos === state.originalParadigm.pos ? "" : "*"}>
          <HTMLSelect
            options={[
              { label: "Іменник", value: "N" },
              { label: "Прикметник", value: "A" },
              { label: "Дієслово", value: "V" },
            ]}
            value={state.paradigm.pos}
            onChange={(e) =>
              dispatch({
                type: "setPos",
                pos: e.target.value,
                id: state.paradigm.id,
              })
            }
          />
        </FormGroup>
      </Section>
      <Section style={{ flexGrow: 1, flexShrink: 1, overflow: "auto" }}>
        {state.paradigm.grammemes.map((g, i) => (
          <GrammemeView
            key={i}
            grammeme={g}
            remove={() =>
              dispatch({
                type: "removeGrammeme",
                id: state.paradigm.id,
                grammemeId: g.id,
              })
            }
            setText={(text) =>
              dispatch({
                type: "updateGrammeme",
                grammeme: { ...g, text },
                id: state.paradigm.id,
              })
            }
            setCategories={(categories) =>
              dispatch({
                type: "updateGrammeme",
                grammeme: { ...g, categories },
                id: state.paradigm.id,
              })
            }
          />
        ))}
        <AddGrammeme
          add={() =>
            dispatch({
              type: "addGrammeme",
              grammeme: createGrammeme(state.newGrammemeId),
              id: state.paradigm.id,
            })
          }
        />
      </Section>
    </Section>
  );
}

export function CenterSpinner() {
  const style = {
    display: "flex",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignContent: "center",
  };

  return (
    <div style={style}>
      <Spinner intent="primary" />
    </div>
  );
}

export function ParadigmView(props: { id: string }) {
  const { paradigm, isPending, error, useUpdateParadigm } = useParadigm(props.id);
  if (props.id === "") {
    return (
      <Section className="App-dictionary-details">
        <div>Select paradigm to view</div>
      </Section>
    );
  }
  if (!paradigm && !isPending) {
    return (
      <Section className="App-dictionary-details">
        <div>Paradigm {props.id} not found</div>
      </Section>
    );
  }
  if (!paradigm && isPending) {
    return (
      <Section className="App-dictionary-details">
        <CenterSpinner />
      </Section>
    );
  }
  if (error) {
    return (
      <Section className="App-dictionary-details">
        <div>{error.toString()}</div>
      </Section>
    );
  }
  return (
    <Section className="App-dictionary-details">
      <LoadedParadigmView paradigm={paradigm!} editable={!isPending} saveParadigm={useUpdateParadigm} />
    </Section>
  );
}
