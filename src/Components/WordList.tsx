import { Button, Card, CardList, Colors, InputGroup, Section, SectionCard, Tag } from "@blueprintjs/core";
import React from "react";
import { Paradigm } from "../Data/Paradigm";
import { CenterSpinner } from "./ParadigmView";
import { useFindParadigms } from "../Data/api";

export function WordList(props: { onSelect: (p: string) => void; currentParadigmId: string | undefined }) {
  const [paradigms, loading, error] = useFindParadigms();

  return (
    <div className="App-dictionary-list">
      <InputGroup large placeholder="Шукати..." rightElement={<Button minimal icon="search" />} />
      <Section style={{ flexGrow: 1, overflow: "scroll" }}>
        <SectionCard padded={false}>
          <CardList compact className="dictionary-table">
            <WordListWrapper
              loading={loading}
              error={error}
              paradigms={paradigms}
              currentParadigmId={props.currentParadigmId}
              onSelect={props.onSelect}
            />
          </CardList>
        </SectionCard>
      </Section>
    </div>
  );
}

function WordListWrapper(props: {
  loading: boolean;
  error: Error | undefined;
  paradigms: Paradigm[];
  currentParadigmId: string | undefined;
  onSelect: (p: string) => void;
}) {
  if (props.loading) {
    return <CenterSpinner />;
  }
  if (props.error) {
    return <div>{props.error.toString()}</div>;
  }
  return (
    <SectionCard padded={false}>
      <CardList compact className="dictionary-table">
        {props.paradigms.map((p, i) => (
          <WordInList key={i} p={p} selected={props.currentParadigmId === p.id} onClick={() => props.onSelect(p.id)} />
        ))}
      </CardList>
    </SectionCard>
  );
}

function WordInList(props: { p: Paradigm; selected: boolean; onClick: () => void }) {
  const bg =
    {
      A: Colors.ORANGE4,
      V: Colors.ROSE4,
      N: Colors.CERULEAN4,
    }[props.p.pos] || Colors.GRAY3;
  return (
    <Card interactive style={props.selected ? { background: Colors.BLUE5 } : {}} compact onClick={props.onClick}>
      <Tag
        style={{
          marginRight: "5pt",
          fontWeight: "bold",
          background: bg,
        }}
      >
        {props.p.pos}
      </Tag>
      <div>{props.p.text}</div>
      <div style={{ flexGrow: 1 }}></div>
      <sub>{props.p.categories}</sub>
    </Card>
  );
}
