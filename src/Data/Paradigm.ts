function ensureString(obj: any, key: string): string {
  const val = obj[key];
  if (typeof val === "string") {
    return val;
  }
  throw new Error(`Value is not a string`);
}

function ensureList(obj: any, key: string): any[] {
  const val = obj[key];
  if (Array.isArray(val)) {
    return val;
  }
  throw new Error(`Value is not an array`);
}

export interface Grammeme {
  id: string;
  text: string;
  categories: string;
}

export function parseGrammeme(obj: any): Grammeme {
  return {
    id: ensureString(obj, "id"),
    text: ensureString(obj, "text"),
    categories: ensureString(obj, "categories"),
  };
}

export function createGrammeme(id: string): Grammeme {
  return { id, text: "", categories: "" };
}

export function compareGrammemes(a: Grammeme, b: Grammeme) {
  return a.id === b.id && a.text === b.text && a.categories === b.categories;
}

export interface Paradigm {
  id: string;
  pos: string;
  text: string;
  categories: string;
  grammemes: Grammeme[];
  modified?: boolean;
}

export function compareParadigms(a: Paradigm, b: Paradigm): boolean {
  return (
    a.id === b.id &&
    a.pos === b.pos &&
    a.text === b.text &&
    a.categories === b.categories &&
    a.grammemes.length === b.grammemes.length &&
    a.grammemes.every((e, i) => compareGrammemes(e, b.grammemes[i]))
  );
}

export function parseParadigm(obj: any): Paradigm {
  return {
    id: ensureString(obj, "id"),
    pos: ensureString(obj, "pos"),
    text: ensureString(obj, "text"),
    categories: ensureString(obj, "categories"),
    grammemes: ensureList(obj, "grammemes").map(parseGrammeme),
  };
}

interface UpdatedGrammeme {
  id: string;
  text?: string;
  categories?: string;
}

interface NewGrammeme {
  text: string;
  categories: string;
}

export interface UpdatedParadigm {
  id: string;
  pos?: string;
  text?: string;
  categories?: string;
  grammemes?: UpdatedGrammeme[];
  removedGrammemes?: string[];
  newGrammemes?: NewGrammeme[];
}

export function makeNewGrammeme(p: Grammeme): NewGrammeme {
  return { text: p.text, categories: p.categories };
}
export function updatedGrammeme(p: Grammeme): UpdatedGrammeme {
  return {
    id: p.id,
    text: p.text,
    categories: p.categories,
  };
}
export function updatedParadigm(p: Paradigm, removedGrammemes?: string[], newGrammemes?: Grammeme[]): UpdatedParadigm {
  return {
    id: p.id,
    pos: p.pos,
    text: p.text,
    categories: p.categories,
    grammemes: p.grammemes.map(updatedGrammeme),
    removedGrammemes: removedGrammemes?.map((e) => e),
    newGrammemes: newGrammemes?.map(makeNewGrammeme),
  };
}
