import { useFetch } from "usehooks-ts";
import { useEffect, useState } from "react";
import { Paradigm, parseParadigm, UpdatedParadigm } from "./Paradigm";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";

let endpoint = "";

export interface User {
  id: string;
  name: string;
  email: string;
}

export function useUser() {
  const { isPending, error, data } = useQuery({
    queryKey: ["user"],
    retry: false,
    queryFn: async () => {
      const res = await fetch("/api/user");
      const a = await res.json();
      return a as User;
    },
  });
  return { isPending, error, user: data };
}

export function logOut() {
  return fetch("/api/logout", { method: "POST" });
}

export function useParadigm(id: string) {
  const query = useQuery({
    queryKey: ["paradigm", id],
    queryFn: async () => {
      const res = await fetch(`/api/paradigm/${id}`);
      const data = await res.json();
      return parseParadigm(data);
    },
  });
  const client = useQueryClient();

  const mutation = useMutation({
    mutationKey: ["paradigm", id],
    mutationFn: async (paradigm: UpdatedParadigm) => {
      const res = await fetch("/api/paradigm", {
        method: "PATCH",
        body: JSON.stringify(paradigm),
        headers: { "Content-Type": "application/json" },
      });
      return await res.json();
    },
    onSuccess: (response) => {
      client.setQueryData(["paradigm", id], parseParadigm(response));
    },
  });

  const useUpdateParadigm = (paradigm: UpdatedParadigm) => {
    mutation.mutate(paradigm);
  };
  return {
    paradigm: query.data,
    isPending: mutation.isPending || query.isPending,
    error: query.error || mutation.error,
    useUpdateParadigm,
  };
}

export function useFindParadigms(): [Paradigm[], boolean, Error | undefined] {
  const url = `${endpoint}/api/paradigms/`;
  const { data, error } = useFetch<[]>(url);
  const [paradigm, setParadigm] = useState<Paradigm[]>([]);
  useEffect(() => {
    if (data) {
      setParadigm(data.map(parseParadigm));
    } else {
      setParadigm([]);
    }
  }, [data]);
  const loading = !data || !!error;
  return [paradigm, loading, error];
}
