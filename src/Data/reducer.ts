import { compareParadigms, Grammeme, Paradigm } from "./Paradigm";

export function InitialState(paradigm: Paradigm) {
  return {
    originalParadigm: paradigm,
    paradigm: {
      id: paradigm.id,
      text: paradigm.text,
      pos: paradigm.pos,
      categories: paradigm.categories,
      grammemes: paradigm.grammemes.map((e) => Object.assign({}, e)),
    } as Paradigm,
    removedGrammemes: [] as string[],
    addedGrammemes: [] as Grammeme[],
    errors: [] as string[],
    _newGrammemeId: 1,
    newGrammemeId: "N1",
    modified: false,
  };
}

export type State = ReturnType<typeof InitialState>;

export type Actions =
  | { type: "reset"; paradigm: Paradigm }
  | { type: "setText"; text: string; id: string }
  | { type: "setPos"; pos: string; id: string }
  | { type: "setCategories"; categories: string; id: string }
  | { type: "removeGrammeme"; grammemeId: string; id: string }
  | { type: "addGrammeme"; grammeme: Grammeme; id: string }
  | { type: "updateGrammeme"; grammeme: Grammeme; id: string };

function addError(state: State, error: string): State {
  return {
    ...state,
    errors: [...state.errors, error],
  };
}

export function modified(state: State): boolean {
  return (
    state.addedGrammemes.length > 0 ||
    state.removedGrammemes.length > 0 ||
    !compareParadigms(state.paradigm, state.originalParadigm)
  );
}

export function reducer(prevState: State, action: Actions): State {
  const nextState = reducer2(prevState, action);
  if (nextState !== prevState) {
    nextState.modified = modified(nextState);
  }
  return nextState;
}

export function reducer2(prevState: State, action: Actions): State {
  switch (action.type) {
    case "reset":
      return InitialState(action.paradigm);
    case "setText":
      if (action.id !== prevState.paradigm.id) {
        return addError(prevState, "Race condition: trying to set text for a wrong paradigm");
      }
      return { ...prevState, paradigm: { ...prevState.paradigm, text: action.text } };

    case "setPos":
      if (action.id !== prevState.paradigm.id) {
        return addError(prevState, "Race condition: trying to set POS for a wrong paradigm");
      }
      return { ...prevState, paradigm: { ...prevState.paradigm, pos: action.pos } };

    case "setCategories":
      if (action.id !== prevState.paradigm.id) {
        return addError(prevState, "Race condition: trying to set categories for a wrong paradigm");
      }
      return { ...prevState, paradigm: { ...prevState.paradigm, categories: action.categories } };

    case "removeGrammeme":
      if (action.id !== prevState.paradigm.id) {
        return addError(prevState, "Race condition: trying to remove grammeme for a wrong paradigm");
      }
      return {
        ...prevState,
        paradigm: {
          ...prevState.paradigm,
          grammemes: prevState.paradigm.grammemes.filter((g) => g.id !== action.grammemeId),
        },
        removedGrammemes: [...prevState.removedGrammemes, action.grammemeId],
      };

    case "addGrammeme":
      if (action.id !== prevState.paradigm.id) {
        return addError(prevState, "Race condition: trying to add grammeme for a wrong paradigm");
      }
      const newGrammemeId = prevState._newGrammemeId + 1;
      return {
        ...prevState,
        paradigm: {
          ...prevState.paradigm,
          grammemes: prevState.paradigm.grammemes.concat(action.grammeme),
        },
        addedGrammemes: [...prevState.addedGrammemes, action.grammeme],
        _newGrammemeId: newGrammemeId,
        newGrammemeId: `N${newGrammemeId}`,
      };

    case "updateGrammeme":
      if (action.id !== prevState.paradigm.id) {
        return addError(prevState, "Race condition: trying to update grammeme for a wrong paradigm");
      }
      return {
        ...prevState,
        paradigm: {
          ...prevState.paradigm,
          grammemes: prevState.paradigm.grammemes.map((p) => {
            if (p.id === action.grammeme.id) {
              return action.grammeme;
            }
            return p;
          }),
        },
        addedGrammemes: prevState.addedGrammemes.map((p) => {
          if (p.id === action.grammeme.id) {
            return action.grammeme;
          }
          return p;
        }),
      };
  }
}
