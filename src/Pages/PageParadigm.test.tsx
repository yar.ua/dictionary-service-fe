import React from "react";
import { render, screen } from "@testing-library/react";
import PageParadigm from "./PageParadigm";

test("renders learn react link", () => {
  render(<PageParadigm />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
