import React, { useEffect, useState } from "react";
import "./PageParadigm.css";
import { Header } from "../Components/Header";
import { WordList } from "../Components/WordList";
import { ParadigmView } from "../Components/ParadigmView";
import { useSearchParams } from "react-router-dom";

export default function PageParadigm() {
  const [searchParams, setSearchParams] = useSearchParams();
  const currentParadigmId = searchParams.get("paradigm") || "";
  const setCurrentParadigmId = (paradigmId: string) => setSearchParams({ paradigm: paradigmId });

  return (
    <div className="App">
      <Header />
      <div className="App-content">
        <WordList onSelect={setCurrentParadigmId} currentParadigmId={currentParadigmId} />
        {currentParadigmId == "" ? <ParadigmView id={"1"} /> : <ParadigmView id={currentParadigmId} />}
      </div>
    </div>
  );
}
